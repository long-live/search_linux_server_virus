# 查杀服务器挖矿病毒


## 一、查找进程并强制杀死相应进程

```
1、查找相关进程
ps -aux | grep kinsing

ps -aux | grep kdevtmpfsi

2、杀死进程
kill -9 你找到的病毒进程PId
```

## 二、查找病毒文件 kinsing、kdevtmpfsi
```
1、查找病毒文件
find / -name kinsing

find / -name kdevtmpfsi

2、强制删除病毒文件
rm -f 病毒文件
```

## 三、查看周期任务cron，不一定在root用户下，有可能在postgres（数据库）、redis（内存数据库）、docker（容器）、PHPTest（PHP单元测试）
```
cd /var/spool/cron //进入周期任务的目录，如果这里有不是自己写的周期，请不要客气，删除之
```

## 四、给postgres用户设置有一定强度的密码

```
passwd postgres
```

## 五、及时更新系统补丁，补上漏洞

```
yum -y update //Centos系列服务器
apt -y update //Debian系列服务器
```

>注意：在第五步完成后，因为时间差的的原因，可能仍然能找到相应进程和文件，重复执行第一步到第三步的操作，直到查不到病毒文件为止